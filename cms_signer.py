from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from flask_cors import CORS
import json
import requests
import base64

app = Flask(__name__)
CORS(app)
api = Api(app)

URL_INICIALIZATION = 'https://fw2.bry.com.br/api/cms-signature-service/v1/signatures/initialize'
URL_FINALIZATION = 'https://fw2.bry.com.br/api/cms-signature-service/v1/signatures/finalize'



class Initialize_Signature(Resource):
    def post(self):
        # Step 1 - Receiving the contents of the digital certificate and the document to be signed.
        data = request.form
        document = request.files['document'].read()

        original_documents = []
        original_documents.append(('originalDocuments[0][content]', document))

        signer_form = {
            'nonce': 1,
            'attached': 'true',
            'profile': 'BASIC',
            'hashAlgorithm': 'SHA256',
            'certificate': data['certificate'],
            'operationType': 'SIGNATURE',
            'originalDocuments[0][nonce]': '1'
        }
        
        print('============= Initializing CMS signature ... =============')

        # Step 2 - Initialization of the signature and production of the signedAttributes artifacts.
        response = requests.post(URL_INICIALIZATION, data=signer_form, files=original_documents, headers={ 'Authorization':request.headers['Authorization'] })
        if response.status_code == 200:
            print(response.json())

        # Step 3 - Send the data for encryption at the frontend.

            return response.json()
        else:
            print(response.text)

class Finalize_Signature(Resource):
    def post(self):

    # Step 4 - Receipt of encrypted data at the frontend.
    
        data = request.form
        document = request.files['document'].read()

        form_finalization = {
            'nonce': 1,
            'attached': 'true',
            'profile': 'BASIC',
            'hashAlgorithm': 'SHA256',
            'certificate': data['certificate'],
            'operationType': 'SIGNATURE'
        }


        form_finalization['finalizations[0][nonce]'] = data['signedNonce']
        form_finalization['finalizations[0][signedAttributes]'] = data['content']
        form_finalization['finalizations[0][signatureValue]'] = data['messageDigest']

        finalizations = []
        finalizations.append(('finalizations[0][document]', document))

        print('============= Finishing CMS signature... =============')

        # Step 5 - Finalization of the signature and obtaining the signed artifact.
        response = requests.post(URL_FINALIZATION, data=form_finalization, files=finalizations, headers={ 'Authorization':request.headers['Authorization'] })
        if response.status_code == 200:
            data = response.json()
            base64_string = data['signatures'][0]['content']
            signature = base64.b64decode(base64_string.encode('utf-8'))
            file = open('./files/assinaturaCMS.p7s', 'wb')
            file.write(signature)
            file.close()
            return response.json()
        else:
            print(response.text)

api.add_resource(Initialize_Signature, '/initializeSignature')
api.add_resource(Finalize_Signature, '/finalizeSignature')

if __name__ == '__main__':
    app.run(debug=True)