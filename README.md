# API de Geração de Assinatura CMS com a BRy Extension

Este é o exemplo backend de integração dos serviços da API de assinatura com clientes baseados em tecnologia Python, que utilizam a BRy Extension. 

Este exemplo apresenta os passos necessários para a geração de assinatura CMS utilizando-se chave privada armazenada em disco.
  - Passo 1: Recebimento do conteúdo do certificado digital e do documento a ser assinado.
  - Passo 2: Inicialização da assinatura e produção dos artefatos signedAttributes.
  - Passo 3: Envio dos dados para cifragem no frontend.
  - Passo 4: Recebimento dos dados cifrados do frontend.
  - Passo 5: Finalização da assinatura e obtenção do artefato assinado.

### Tech

O exemplo utiliza das bibliotecas Python abaixo:
* [Requests] - HTTP for Humans!
* [Flask] - Flask (source code) is a Python Web framework designed to receive HTTP requests.
* [Flask-restful] - Flask-RESTful is an extension for Flask that adds support for quickly building REST APIs
* [Flask-Cors] - A Flask extension for handling Cross Origin Resource Sharing (CORS), making cross-origin AJAX possible.
* [Python-3.6] - Python 3.6

**Observação**

Esta API em Python deve ser executada juntamente com o FrontEnd, desenvolvido em React, para que seja feita a cifragem dos dados com a extensão para web "BRy Extension"

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência e instale as dependências. Utilizamos o Python versão 3.6 e pip3 para a instalação das dependências.

Comandos:

Instalar Flask:

    -pip3 install Flask

Instalar Flask-Cors

    -pip3 install -U flask-cors

Instalar Flask-Restful:

    -pip3 install flask-restful

Executar programa:

    -python3 cms_signer.py



   [Requests]: <https://pypi.org/project/requests/>
   [Flask]: <https://palletsprojects.com/p/flask/>
   [Flask-restful]:<https://flask-restful.readthedocs.io/en/latest/> 
   [Flask-Cors]: <https://flask-cors.readthedocs.io/en/latest/>
   [Python-3.6]: <https://www.python.org/>
